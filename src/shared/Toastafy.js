import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

 const types = ["success", "info", "warning", "error"];

const toastNotify = (message, type) => {
  toast(message, {
    type: types[type],
    autoClose: 4000,
    position: toast.POSITION.BOTTOM_CENTER,
  });
}
export default toastNotify;