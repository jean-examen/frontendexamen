import "./index.scss";

function Loader(props) {
  return (
    <div
      style={{
        width: props.w,
        height: props.h,
        backgroundColor: props.bgColor,
      }}
      className="App-loader"
    >
      <div hidden={true} class="loader"></div>
    </div>
  );
}
export default Loader;
