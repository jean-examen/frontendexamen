import { Link, NavLink } from "react-router-dom";
import "./index.css";

function navbar() {
  return (
    <header>
      <nav className="main-nav">
        <input type="checkbox" id="isChecked" />
        <label htmlFor="isChecked" className="menu-btn">
          <i className="fas fa-bars"></i>
        </label>
        <a href="index.html" className="logo">
          LC DENTAL
        </a>
        <ul className="navlinks">
          <li>
            <NavLink to="/">INICIO</NavLink>
          </li>
          <li>
            <NavLink name="patient_list" to="/patient/list">
              PACIENTES
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
}
export default navbar;
