import { ErrorMessage } from "@hookform/error-message";
import { yupResolver } from "@hookform/resolvers/yup";
import appointmentSchema from "./validations/appointmentFormValidations";
import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import userSchema from "../Pacient/validations/userFormValidations";

import axios from "axios";
import { useParams, useLocation, useNavigate } from "react-router-dom";
import { getValue } from "@mui/system";
import { sweetConfrim } from "./service/appointmentService";

function EditForm() {

  const navigate =useNavigate();
  const { id } = useParams();


  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(appointmentSchema),
  });

  const [appointment, setAppointment] = useState("");

  useEffect(() => {
    const options = {
      method: "GET",
      url: "http://localhost/api Examen/appointment/findById/" + id,
    };

    axios
      .request(options)
      .then(function (response) {
         console.log(response.data)
        setAppointment(response.data);

      })
      .catch(function (error) {
        console.error(error);
      });
  }, []);

  const onSubmit = (dataForm) => {


    const options = {
      method: "PUT",
      url: `http://localhost/api Examen/appointment/update/${id}`,
      data: dataForm,
    };

    axios
      .request(options)
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
        alert("Error al actualizar cita");
      }).finally(()=>navigate(-1));
    
    sweetConfrim();


  };

  return (
    <React.Fragment>
      <div id="register-body" className="register-body">
        <div className="container">
          <div className=" col-xs-12 col-sm-12 col-md-10 col-lg-7 mx-auto">
            <div className="card ">
              <div>
                <h4 className=" mt-2 d-flex justify-content-center title-header ">
                  Editar Cita
                </h4>
                <hr />
              </div>
              <div className="card-body pl-5 pr-5 ">
                {appointment[0] ? (
                  <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="d-flex">
                      <div className="row ">
                        <div className="col-sm-12 col-md-6 col-lg-6">
                          <div className="form-group">
                            <label htmlFor="">Razon</label>
                            <input
                              defaultValue={
                                appointment[0] ? appointment[0].reason : ""
                              }
                              type="text"
                              {...register("reason", { required: true })}
                              style={{
                                border: errors.reason
                                  ? "3px solid rgb(216, 52, 79)"
                                  : "",
                              }}
                              placeholder={
                                errors.reason
                                  ? "Este campo es requerido*"
                                  : "Ingrese la razon de la cita"
                              }
                            />
                          </div>
                        </div>
                        <div className="col-sm-12 col-md-6 col-lg-6">
                          <div className="form-group">
                            <label htmlFor="">Fecha de Cita</label>
                            <input
                              type="datetime-local"
                              defaultValue={
                                appointment[0]
                                  ? appointment[0].date_appointment
                                  : ""
                              }
                              {...register("date_appointment", {
                                required: true,
                              })}
                              style={{
                                border: errors.date_appointment
                                  ? "3px solid rgb(216, 52, 79)"
                                  : "",
                              }}
                              placeholder={
                                errors.date_appointment
                                  ? "Este campo es requerido*"
                                  : "Ingrese la fecha"
                              }
                            />
                          </div>
                        </div>
                      </div>
                    </div>

                    <button
                      className="btn btn-cgray btn-block d-flex justify-content-center align-content-between"
                      type="submit"
                    >
                      <i className="material-symbols-sharp mr-1">save</i>
                      Agregar
                    </button>
                  </form>
                ) : (
                  "...cargando datos"
                )}
              </div>
              <div className="panel-footer d-flex justify-content-center"></div>
            </div>
          </div>
        </div>
        <div></div>
      </div>
      {/* <Footer></Footer> */}
    </React.Fragment>
  );
}

export default EditForm;
