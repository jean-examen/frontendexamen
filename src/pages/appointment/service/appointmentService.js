import axios from "axios";
import Swal from "sweetalert2";

export const edit = (id) => {
  const options = {
    method: "GET",
    url: "http://localhost/api Examen/appointment/findById/" + id,
  };

  return axios
    .request(options)
    .then(function (res) {
      const response = res.data;
      return response;
    })
    .catch(function (error) {
      console.error(error);
    });
};

export const deleted = (id) => {
  console.log("entro a eliminar");
  const options = {
    method: "DELETE",
    url: "http://localhost/api Examen/appointment/delete/" + id,
  };

  return axios
    .request(options)
    .then(function (res) {
      const response = res.data;
      console.log(res.data);
      return response;
    })
    .catch(function (error) {
      console.error(error);
    });
};

export const saveEdit = (dataForm, id, navigate) => {
  const options = {
    method: "PUT",
    url: `http://localhost/api Examen/appointment/update/${id}`,
    data: dataForm,
  };

  axios
    .request(options)
    .then((response) => {
      console.log("Data form", dataForm);
    })
    .catch((error) => {
      console.log(error);
      alert("Error al actualizar cita");
    })
    .finally(() => navigate(-1));
};

// ----------------------------- SWEET ALERT ----------------------------------------------

export const swalert = (title, message, confirm) => {
  return {
    title: title,
    text: message,
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: confirm,
  };
};

export const sweetConfrim = () => {
  Swal.fire({
    position: "top-end",
    icon: "success",
    title: "Tus cambios estan guardados",
    showConfirmButton: false,
    timer: 1500,
  });
};

export const editPacient = (navigate, id_user) => {
  Swal.fire(swalert("Editar", "Esta seguro desea editar este registro")).then(
    (result) => {
      if (result.isConfirmed) {
        navigate(`/appointment/update/${id_user}`);
      }
    }
  );
};
