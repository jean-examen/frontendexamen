import Swal from "sweetalert2";
import React from "react";
import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "./style.css";
import { useParams } from "react-router-dom";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { useForm } from "react-hook-form";
import Tippy from "@tippyjs/react";
import Loader from "../../shared/loader";
import { deleted, swalert } from "../appointment/service/appointmentService";

export default function Appointment() {
  const [reason, setReason] = useState("");
  const [date, setDateAppointment] = useState("");
  const [appointment, setAppointment] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [nameError, setNameError] = useState({
    error: false,
    helperText: "",
  });

  const { id } = useParams();
  const {
    handleSubmit,
    formState: { errors },
  } = useForm({});

  useEffect(() => {
    getAppointmentData(id);
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleReasonChange = (event) => {
    setReason(event.target.value);
  };

  const handleDateChange = (event) => {
    setDateAppointment(event.target.value);
  };

  const getAppointmentData = (id) => {
    var form = {
      id: id,
      date_appointment: "",
    };

    fetch("http://localhost/api Examen/appointment/history/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form),
    })
      .then((res) => res.json())
      .then((res) => {
        setAppointment(res);
        console.log(appointment);
      })
      .catch(function () {});
  };

  async function saveAppointment() {
    console.log("aqui toy");
    const form = {
      id_patient: id,
      date_appointment: date,
      reason: reason,
    };

    console.log(form);
    return await axios
      .post("http://localhost/api Examen/appointment/create", JSON.stringify(form))
      .then((res) => console.log(res))
      .catch((error) => {
        console.error("Error al guardar: ", error);
      });
  }

  const editAppointment = (id) => {
    Swal.fire(
      swalert("", "Esta seguro que desea editar esta cita", "Si, esta bien")
    ).then((result) => {
      if (result.isConfirmed) {
        window.location.href = `/appointment/edit/${id}`;
      }
    });
  };

  const deleteCallback = (id_appointment) => {
    Swal.fire(
      swalert(
        "Esta acción es inrrebercible",
        "Esta seguro que desea cancelar esta cita",
        "Si, esta bien"
      )
    ).then((result) => {
      if (result.isConfirmed) {
        deleted(id_appointment).then(() => {
          Swal.fire(
            "Cacelada!",
            "El registro se actualizó exitosamente",
            "success"
          );
          getAppointmentData();
        });
      }
    });
  };

  return (
    <>
      <div>
        {/* {appointment[0] ? ( */}
        <div className="App mt-5">
          <div className="col-md-10 mx-auto">
            <div className="row_actions">
              <Tippy content="Lista de pacientes">
                <Link to="/patient/list">
                  <button className="btn btn_ligth_blur">
                    <i class="fa-solid fa-user-large"></i>
                  </button>
                </Link>
              </Tippy>
              <Tippy content="Agendar Cita">
                <button
                  className="btn btn_ligth_blur"
                  onClick={handleClickOpen}
                >
                  <i class="fa-solid fa-calendar-plus"></i>
                </button>
              </Tippy>

              <div className="title_name">
                <h3>
                  {appointment[0]
                    ? appointment[0].name + " " + appointment[0].last_name
                    : ""}
                </h3>
              </div>
            </div>
            <table>
              <thead>
                <tr>
                  <th>Asunto</th>
                  <th>Fecha</th>
                  <th className="AlignTh">ACCIONES</th>
                </tr>
              </thead>
              <tbody>
                {appointment.map((data, index) => {
                  return (
                    <tr key={index}>
                      <td>{data.reason}</td>
                      <td>{data.date_appointment}</td>
                      <div className="Actions">
                        <td>
                          <Tippy content="Editar Cita">
                            <button
                              className="btn btn_ligth_blur"
                              onClick={() => editAppointment(data.id)}
                            >
                              <i class="fa-solid fa-user-pen"></i>
                            </button>
                          </Tippy>
                          <Tippy content="Cancelar">
                            <button
                              className="btn btn-danger"
                              onClick={() => deleteCallback(data.id)}
                            >
                              <i class="fa-solid fa-trash-can"></i>
                            </button>
                          </Tippy>
                        </td>
                      </div>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
          <Dialog open={open} onClose={handleClose}>
            <form action="" onSubmit={saveAppointment}>
              <DialogTitle>Agendar</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  Ingresar motivos de la cita y seleccionar la fecha de la cita
                </DialogContentText>

                <TextField
                  autoFocus
                  margin="dense"
                  id="standard-basic"
                  label="Razon"
                  type=""
                  fullWidth
                  variant="standard"
                  onChange={handleReasonChange}
                  error={nameError.error}
                  helperText={nameError.helperText}
                  required
                />
                <TextField
                  autoFocus
                  margin="normal"
                  id="name"
                  label=""
                  type="datetime-local"
                  fullWidth
                  variant="standard"
                  onChange={handleDateChange}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={handleSubmit(handleClose)}>Cancel</Button>
                <Button type="submit">Agendar</Button>
              </DialogActions>
            </form>
          </Dialog>
        </div>
        {/* // ) : (
        //   <Loader></Loader>
        // )} */}
      </div>
    </>
  );
}
