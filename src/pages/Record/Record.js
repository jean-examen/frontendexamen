import React from "react";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import Tippy from "@tippyjs/react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  Button,
  Input,
} from "reactstrap";
import { getSchedule } from "./service/RecordService";
import Gallery from "../Galery/App";

export default function Record() {
  const { id } = useParams();
  const [patientData, setPatientData] = useState("");
  const [modalDetail, setModalDetail] = useState(false);
  const [detail, setDetail] = useState("");
  const [appointmentSchedule, setAppointmentSchedule] = useState([]);

  useEffect(() => {
    document.title = `Expediente`;
    getSchedule(id, "").then(function (res) {
      setAppointmentSchedule(res);
      console.log("Citas", res);
    });
    const options = {
      method: "GET",
      url: `http://localhost/api/patient/findById/${id}`,
    };

    axios
      .request(options)
      .then(function (response) {
        setPatientData(response.data[0]);
        console.log(patientData);
      })
      .catch(function (error) {
        console.error(error);
      });
  }, []);
  const openCloseModalDetail = () => {
    setModalDetail(!modalDetail);
  };

  const saveDatail = () => {
    alert("Informacion guardada");
    openCloseModalDetail();
  };

  function handChange(name, value) {
    switch (name) {
      case "detail":
        setDetail(value);
        break;
    }
  }

  return (
    <div>
      <>
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-10 mx-auto mt-3">
          <div className="row_actions">
            <Tippy content="Historial">
              <button className="btn btn_ligth_blur">
                <i class="fa-solid fa-layer-group"></i>{" "}
              </button>
            </Tippy>
            <Tippy content="Agregar detalle">
              <button
                className="btn btn_ligth_blur"
                onClick={openCloseModalDetail}
              >
                <i class="fa-solid fa-info"></i>{" "}
              </button>
            </Tippy>

            <div className="title_name">
              <h3>{patientData.name + " " + patientData.last_name}</h3>
            </div>
          </div>
          <div class="row m-1">
            <div class="container-fluid col-sm-12 col-md-12 col-lg-12 col-xl-6">
              <div className="card_container">
                <h3>Datos</h3>
                <ul>
                  <li>
                    <strong>CÉDULA: </strong> {patientData.id_card}
                  </li>

                  <li>
                    <strong>DIRECCIÓN: </strong> {patientData.location}
                  </li>
                  <li>
                    <strong>TELÉFONO: </strong> {patientData.phone_number}
                  </li>
                  <li>
                    <strong>EMAIL: </strong> {patientData.email}
                  </li>
                  <hr></hr>
                  <li>
                    <strong>DETALLES: </strong> {patientData.comments}
                  </li>
                </ul>
              </div>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
              <div className="card_container">
                <table>
                  <thead>
                    <tr>
                      <th>Asunto</th>
                      <th>Fecha</th>
                      <th className="AlignTh">ACCIONES</th>
                    </tr>
                  </thead>
                  <tbody>
                    {appointmentSchedule.map((data, index) => {
                      return (
                        <tr key={index}>
                          <td>{data.reason}</td>
                          <td>{data.date_appointment}</td>
                          <div className="Actions">
                            <td>
                              <Tippy content="Editar Cita">
                                <button
                                  className="btn btn_ligth_blur"
                                  onClick={""}
                                >
                                  <i class="fa-solid fa-user-pen"></i>
                                </button>
                              </Tippy>
                              <Tippy content="Cancelar">
                                <button className="btn btn-danger" onClick={""}>
                                  <i class="fa-solid fa-trash-can"></i>
                                </button>
                              </Tippy>
                            </td>
                          </div>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <div className="content mt-3">
          <div className="App_factura bg-primary col-md-10 mx-auto">
            <div className="col-md-12"></div>

            <div className="col-md-12"></div>
          </div>
        </div>
      </>
      <Modal isOpen={modalDetail}>
        <ModalHeader>Ingrese los detalles del paciente</ModalHeader>
        <ModalBody>
          <FormGroup>
            <Input
              type="textarea"
              id="detail"
              value={detail}
              onChange={(e) => handChange("detail", e.target.value)}
            />
          </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={openCloseModalDetail}>
            Cancelar
          </Button>

          <Button color="primary" onClick={saveDatail}>
            Agregar
          </Button>
        </ModalFooter>
      </Modal>
      <Gallery></Gallery>
    </div>
  );
}
