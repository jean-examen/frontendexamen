import axios from "axios";
import Swal from "sweetalert2";

export const getSchedule = (id, date_appointment) => {

  var form = {
    id: id,
    date_appointment: date_appointment,
    flag: 1
  };

  return new Promise(function(resolve, reject) {
    fetch("http://localhost/api/appointment/listData", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form),
    })
      .then((res) => res.json())
      .then((res) => {
        resolve(res);
      })
      .catch(function () {
      });
  })
  
};



// ----------------------------- SWEET ALERT ----------------------------------------------

export const swalert = (title, message, confirm) => {
  return {
    title: title,
    text: message,
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: confirm,
  };
};

export const sweetConfrim = () => {
  Swal.fire({
    position: "top-end",
    icon: "success",
    title: "Tus cambios estan guardados",
    showConfirmButton: false,
    timer: 1500,
  });
};

export const editPacient = (navigate, id_user) => {
  Swal.fire(swalert("Editar", "Esta seguro desea editar este registro")).then(
    (result) => {
      if (result.isConfirmed) {
        navigate(`/appointment/update/${id_user}`);
      }
    }
  );
};
