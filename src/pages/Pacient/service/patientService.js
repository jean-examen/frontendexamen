import axios from "axios";
import Swal from "sweetalert2";

export const list = () => {
  const options = {
    method: "GET",
    withCredentials: false,
    url: "http://localhost/api Examen/patient/list/",
  };

  return axios.request(options).then((res) => {
    const response = res.data;
    return response;
  });
};

export const search = (searchValue) => {
  const options = {
    method: "GET",
    withCredentials: false,
    url: "http://localhost/api Examen/patient/search/" + searchValue,
  };

  return axios.request(options).then((res) => {
    const response = res.data;
    return response;
    //setPatients(response);
  });
};

export const edit = (id) => {
  const options = {
    method: "GET",
    url: "http://localhost/api Examen/patient/findById/" + id,
  };

  return axios
    .request(options)
    .then(function (res) {
      const response = res.data;
      return response;
    })
    .catch(function (error) {
      console.error(error);
    });
};

export const save = (dataForm) => {
  const options = {
    method: "POST",
    url: "http://localhost/api Examen/patient/create",
    data: dataForm,
  };

  return axios
    .request(options)
    .then(function (res) {
      const response = res.data;
      return response;
    })
    .catch(function (error) {
      console.error(error);
    });
};

export const deleted = (id) => {
  console.log("entro a eliminar");
  const options = {
    method: "DELETE",
    url: "http://localhost/api Examen/patient/delete/" + id,
  };

  return axios
    .request(options)
    .then(function (res) {
      const response = res.data;
      console.log(res.data);
      return response;
    })
    .catch(function (error) {
      console.error(error);
    });
};

export const saveEdit = (dataForm, id) => {
  const options = {
    method: "PUT",
    url: "http://localhost/api Examen/patient/update/" + id,
    data: dataForm,
  };

  return axios
    .request(options)
    .then(function (res) {
      const response = res.data;
      return response;
    })
    .catch(function (error) {
      console.error(error);
    });
};

// ----------------------------- SWEET ALERT ----------------------------------------------

export const swalert = (action, message) => {
  return {
    title: message,
    icon: "warning",
    background: "#212c3b",
    color: "#c7c7c7",
    showCancelButton: true,
    confirmButtonColor: "#808080",
    cancelButtonColor: "#c51843",
    confirmButtonText: action,
  };
};

export const editPacient = (navigate, id_user) => {
  Swal.fire(swalert("Editar", "Esta seguro desea editar este registro")).then(
    (result) => {
      if (result.isConfirmed) {
        navigate(`/patient/edit/${id_user}`);
      }
    }
  );
};

export const sweetConfrim = () => {
  Swal.fire({
    position: "top-end",
    icon: "success",
    title: "Tus cambios estan guardados",
    showConfirmButton: false,
    timer: 1500,
  });
};