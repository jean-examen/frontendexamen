import { ErrorMessage } from "@hookform/error-message";
import { yupResolver } from "@hookform/resolvers/yup";
import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import userSchema from "../Pacient/validations/userFormValidations";
import "../Pacient/style.css";
import { useParams, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Loader from "../../shared/loader/index";
import { edit, saveEdit } from "./service/patientService";

import toastNotify from "../../shared/Toastafy";

function EditForm() {
  const { id } = useParams();
  const navigate = useNavigate();

  const intitialState = {
    id_card: "",
    name: null,
    last_name: null,
    email: null,
    phone_number: null,
    location: null,
    payer: null,
    comments: null,
  };

  const {
    register,
    handleSubmit,
    getValues,
    setValue,

    formState: { errors },
  } = useForm({
    resolver: yupResolver(userSchema),
  });

  const [patient, setPatient] = useState(intitialState);

  useEffect(() => {
    edit(id).then((data) => {
      setPatient(data);
    });
  }, [id]);

  const onSubmit = (dataForm) => {
    console.log(dataForm);

    saveEdit(dataForm, id)
      .then((res) => {
        console.log(res.data);
      })
      .catch(function (error) {
        console.error(error);
      })
      .finally(() => {
        toastNotify("Agregado", 0);
        navigate(`/patient/list/`);
      });
  };

  return (
    <React.Fragment>
      <div>
        {patient[0] ? (
          <div id="register-body" className="register-body mt-5">
            <div className="container">
              <div className=" col-xs-12 col-sm-12 col-md-10 col-lg-7 mx-auto">
                <div className="card ">
                  <div>
                    <h4 className=" mt-2 d-flex justify-content-center title-header ">
                      Editar Paciente
                    </h4>
                    <hr />
                  </div>
                  <div className="card-body pl-5 pr-5 ">
                    <form onSubmit={handleSubmit(onSubmit)}>
                      <div className="d-flex">
                        <div className="row ">
                          <div className="col-sm-12 col-md-6 col-lg-6">
                            <div className="form-group">
                              <label htmlFor="">Nombre</label>
                              <input
                                defaultValue={patient[0] ? patient[0].name : ""}
                                type="text"
                                {...register("name", { required: true })}
                                style={{
                                  border: errors.name
                                    ? "3px solid rgb(216, 52, 79)"
                                    : "",
                                }}
                                placeholder={
                                  errors.name
                                    ? "Este campo es requerido*"
                                    : "Ingrese su nombre"
                                }
                              />
                              <small name="error_message">
                                <ErrorMessage
                                  errors={errors}
                                  name="name"
                                  render={({ message }) => <p>{message}</p>}
                                />
                              </small>
                            </div>
                          </div>
                          <div className="col-sm-12 col-md-6 col-lg-6">
                            <div className="form-group">
                              <label htmlFor="">Apellido</label>
                              <input
                                type="text"
                                defaultValue={
                                  patient[0] ? patient[0].last_name : []
                                }
                                {...register("last_name", { required: true })}
                                style={{
                                  border: errors.last_name
                                    ? "3px solid red"
                                    : "",
                                }}
                                placeholder={
                                  errors.last_name
                                    ? "Este campo es requerido*"
                                    : "Ingrese su apellido"
                                }
                              />
                              <small name="last_name_error_message">
                                <ErrorMessage
                                  errors={errors}
                                  name="last_name"
                                  render={({ message }) => <p>{message}</p>}
                                />
                              </small>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="d-flex">
                        <div className="row ">
                          <div className="col-sm-12 col-md-6 col-lg-6">
                            <div className="form-group">
                              <label htmlFor="">Cedula</label>
                              <input
                                type="text"
                                defaultValue={
                                  patient[0] ? patient[0].id_card : []
                                }
                                {...register("id_card", { required: true })}
                                style={{
                                  border: errors.id_card ? "3px solid red" : "",
                                }}
                                placeholder={
                                  errors.id_card
                                    ? "Este campo es requerido*"
                                    : "Ingrese su nombre"
                                }
                              />
                              <small name="id_error_message">
                                <ErrorMessage
                                  errors={errors}
                                  name="id_card"
                                  render={({ message }) => <p>{message}</p>}
                                />
                              </small>
                            </div>
                          </div>
                          <div className="col-sm-12 col-md-6 col-lg-6">
                            <div className="form-group">
                              <label htmlFor="">Telefono</label>
                              <input
                                type="text"
                                defaultValue={
                                  patient[0] ? patient[0].phone_number : []
                                }
                                {...register("phone_number", {
                                  required: true,
                                })}
                                style={{
                                  border: errors.phone_number
                                    ? "3px solid rgb(216, 52, 79)"
                                    : "",
                                }}
                                placeholder={
                                  errors.phone_number
                                    ? "Este campo es requerido*"
                                    : "Ingrese su apellido"
                                }
                              />
                              <small name="phone_error_message">
                                <ErrorMessage
                                  errors={errors}
                                  name="phone_number"
                                  render={({ message }) => <p>{message}</p>}
                                />
                              </small>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="form-group">
                        <label htmlFor="">Correo</label>
                        <input
                          type="text"
                          defaultValue={patient[0] ? patient[0].email : []}
                          {...register("email", { required: true })}
                          style={{
                            border: errors.email
                              ? "3px solid rgb(216, 52, 79)"
                              : "",
                          }}
                          placeholder={
                            errors.email
                              ? "Este campo es requerido*"
                              : "Correo: ejemplo@dominio.com"
                          }
                        />
                        <small name="email_error_message">
                          <ErrorMessage
                            errors={errors}
                            name="email"
                            render={({ message }) => <p>{message}</p>}
                          />
                        </small>
                      </div>

                      <div className="d-flex">
                        <div className="row ">
                          <div className="col-sm-12 col-md-6 col-lg-6">
                            <div className="form-group">
                              <label htmlFor="">Direccion</label>
                              <input
                                type="text"
                                defaultValue={
                                  patient[0] ? patient[0].location : []
                                }
                                {...register("location", { required: true })}
                                style={{
                                  border: errors.location
                                    ? "3px solid rgb(216, 52, 79)"
                                    : "",
                                }}
                                placeholder={
                                  errors.location
                                    ? "Este campo es requerido*"
                                    : "Ingrese su nombre"
                                }
                              />
                            </div>
                          </div>
                          <div className="col-sm-12 col-md-6 col-lg-6">
                            <div className="form-group">
                              <label htmlFor="">Responsable de pago</label>
                              <input
                                type="text"
                                defaultValue={
                                  patient[0] ? patient[0].payer : []
                                }
                                {...register("payer", { required: false })}
                                style={{
                                  border: errors.payer
                                    ? "3px solid rgb(216, 52, 79)"
                                    : "",
                                }}
                                placeholder={
                                  errors.payer
                                    ? "test*"
                                    : "Ingrese el responsable de pago"
                                }
                              />
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="form-group">
                        <label htmlFor="">Comentario</label>
                        <input
                          type="text"
                          defaultValue={patient[0] ? patient[0].comments : []}
                          {...register("comments", { required: false })}
                          style={{
                            border: errors.comments
                              ? "3px solid rgb(216, 52, 79)"
                              : "",
                          }}
                          placeholder={
                            errors.comments
                              ? "Este campo es requerido*"
                              : "Agregue un comentario"
                          }
                        />
                        <small name="error_message">
                          <ErrorMessage
                            errors={errors}
                            name="comments"
                            render={({ message }) => <p>{message}</p>}
                          />
                        </small>
                      </div>

                      <button
                        name="btn-add"
                        className="btn btn-cgray btn-block d-flex justify-content-center align-content-between"
                        type="submit"
                      >
                        <i className="material-symbols-sharp mr-1">save</i>
                        Agregar
                      </button>
                    </form>
                  </div>
                  <div className="panel-footer d-flex justify-content-center"></div>
                </div>
              </div>
            </div>
            <div></div>
          </div>
        ) : (
          <Loader></Loader>
        )}
      </div>
    </React.Fragment>
  );
}
export default EditForm;
