import "./patient.scss";
import { useEffect, useMemo, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Tippy from "@tippyjs/react";
import "tippy.js/dist/tippy.css";
import Loader from "../../shared/loader/index";
import {
  list,
  search,
  deleted,
  editPacient,
  deletePacient,
  swalert,
  sweetConfrim
} from "./service/patientService";
import Pagination from "../../shared/Pagination/Pagination";
import Swal from "sweetalert2";

let PageSize = 5;
let searchvalue = "";

function PatientList() {
  const getid = (id_patient) => {
    sessionStorage.setItem("id_patient", id_patient);
  };

  const [render, setRender] = useState(false);
  const [patients, setPatients] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const currentTableData = useMemo(() => {
    const firstPageIndex = (currentPage - 1) * PageSize;
    const lastPageIndex = firstPageIndex + PageSize;
    return patients.slice(firstPageIndex, lastPageIndex);
  }, [currentPage, patients]);

  const navigate = useNavigate();

  useEffect(() => {
    document.title= `Pacientes`;
    console.log("ola");
    list().then((PatientList) => {
      setPatients(PatientList);
      setRender(false);
    });
  }, [render]);

  const searchOnchange = (event) => {
    const value = event.target.value;
    console.log(value);
    searchvalue = value;
    search(searchvalue).then((p) => {
      setPatients(p);
    });
  };

  const del = (id) => {
    Swal.fire(swalert("Eliminar", "Esta seguro desea eliminar este registro"))
      .then((result) => {
        deleted(id).then((res) => {
          setRender(true);
        });
      })
      .finally(() => {
        Swal.fire(
          "Eliminado",
          "El paciente se elimino correctamente",
          "success"
        );
      });
  };

  return (
    <div>
      {patients[0] ? (
        <div className="App mt-5">
          <div className="col-md-10 mx-auto">
            <div className="row_action">
              <Link to="/patient/create">
                <button className="btn btn_ligth_blur">
                  <i className="fa-solid fa-user-plus"></i>
                </button>
              </Link>
              <input
                className="input_search"
                placeholder="Buscar..."
                type="text"
                name="search"
                onChange={searchOnchange}
              />
            </div>
            <table id="table_patients" name="table_patients">
              <thead>
                <tr>
                  <th>CEDULA</th>
                  <th>NOMBRE</th>
                  <th>APELLIDO</th>
                  <th>TELÉFONO</th>
                  <th>EMAIL</th>
                  <th>ACCIONES</th>
                </tr>
              </thead>
              <tbody>
                {currentTableData.map((data, index) => {
                  return (
                    <tr key={index}>
                      <td>{data.id_card}</td>
                      <td>{data.name}</td>
                      <td>{data.last_name}</td>
                      <td>{data.phone_number}</td>
                      <td>{data.email}</td>
                      <td>
                        <Tippy content="Editar">
                          <button
                            className="btn btn_ligth_blur"
                            onClick={() => editPacient(navigate, data.id_user)}
                          >
                            <i className="fa-solid fa-user-pen"></i>
                          </button>
                        </Tippy>
                        <Tippy content="Expediente">
                          <Link to={{ pathname: `/record/${data.id_user}` }}>
                            <button className="btn btn_ligth_blur">
                              <i className="fa-solid fa-book-medical"></i>
                            </button>
                          </Link>
                        </Tippy>
                        <Tippy content="Citas">
                          <Link
                            to={{ pathname: `/appointment/${data.id_user}` }}
                          >
                            <button
                              className="btn btn_ligth_blur"
                              onClick={() => getid(data.id_user)}
                            >
                              <i className="fa-solid fa-calendar-check"></i>
                            </button>
                          </Link>
                        </Tippy>

                        <Tippy content="Factura">
                          <Link to={{ pathname: `/factura/${data.id_user}` }}>
                            <button className="btn btn_ligth_blur">
                              <i className="fa-solid fa-file-invoice-dollar"></i>
                            </button>
                          </Link>
                        </Tippy>
                        <Tippy content="Eliminar">
                          <button
                            className="btn btn_red"
                            onClick={() => del(data.id_user)}
                          >
                            <i className="fa-solid fa-trash-can"></i>
                          </button>
                        </Tippy>
                        <span className="tooltip">oli</span>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>

            <div className="row_actions">
              <div>
                <div className="d-flex mt-3">
                  <Pagination
                    className="pagination-bar"
                    currentPage={currentPage}
                    totalCount={patients.length}
                    pageSize={PageSize}
                    onPageChange={(page) => setCurrentPage(page)}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <Loader></Loader>
      )}
    </div>
  );
}

export default PatientList;
