import { ErrorMessage } from "@hookform/error-message";
import { yupResolver } from "@hookform/resolvers/yup";
import React from "react";
import { useForm } from "react-hook-form";
import userSchema from "../Pacient/validations/userFormValidations";
import "../Pacient/style.css";
import { useNavigate } from "react-router-dom";
import { save } from "./service/patientService";
import toastNotify from "../../shared/Toastafy";
import {
  sweetConfrim
} from "./service/patientService";

function RegisterForm() {
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    getValues,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(userSchema),
  });

  const onSubmit = (dataForm) => {
    console.log(dataForm);
    save(dataForm)
      .then((res) => {
        console.log(res.data);
      })
      .finally(() => {
        toastNotify("Agregado", 0);
        navigate("/patient2/list");
      });

      sweetConfrim();
  };

  return (
    <React.Fragment>
      <div>
        <div id="register-body" className="register-body mt-5">
          <div className="container">
            <div className=" col-xs-12 col-sm-12 col-md-10 col-lg-7 mx-auto">
              <div className="card ">
                <div>
                  <h3 className=" p-3 d-flex justify-content-center title-header ">Agregar Paciente</h3>
                  <hr />
                </div>

                <div className="card-body pl-5 pr-5 ">
                  <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="d-flex">
                      <div className="row ">
                        <div className="col-sm-12 col-md-6 col-lg-6">
                          <div className="form-group">
                            <label htmlFor="">Nombre</label>
                            <input
                              type="text"
                              {...register("name", { required: true })}
                              onChangeCapture={() =>
                                setValue("payer", getValues("name"))
                              }
                              style={{
                                border: errors.name
                                  ? "3px solid rgb(216, 52, 79)"
                                  : "",
                              }}
                              placeholder={
                                errors.name
                                  ? "Este campo es requerido*"
                                  : "Ingrese su nombre"
                              }
                            />
                          </div>
                        </div>
                        <div className="col-sm-12 col-md-6 col-lg-6">
                          <div className="form-group">
                            <label htmlFor="">Apellido</label>
                            <input
                              type="text"
                              {...register("last_name", { required: true })}
                              style={{
                                border: errors.last_name
                                  ? "3px solid rgb(216, 52, 79)"
                                  : "",
                              }}
                              placeholder={
                                errors.last_name
                                  ? "Este campo es requerido*"
                                  : "Ingrese su apellido"
                              }
                            />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="d-flex">
                      <div className="row ">
                        <div className="col-sm-12 col-md-6 col-lg-6">
                          <div className="form-group">
                            <label htmlFor="">Cédula</label>
                            <input
                              type="text"
                              {...register("id_card", { required: true })}
                              style={{
                                border: errors.id_card
                                  ? "3px solid rgb(216, 52, 79)"
                                  : "",
                              }}
                              placeholder={
                                errors.id_card
                                  ? "Este campo es requerido*"
                                  : "Ingrese su cedula"
                              }
                            />
                            <small>
                              <ErrorMessage
                                errors={errors}
                                name="id_card"
                                render={({ message }) => <p>{message}</p>}
                              />
                            </small>
                          </div>
                        </div>
                        <div className="col-sm-12 col-md-6 col-lg-6">
                          <div className="form-group">
                            <label htmlFor="">Teléfono</label>
                            <input
                              type="text"
                              {...register("phone_number", { required: true })}
                              style={{
                                border: errors.phone_number
                                  ? "3px solid rgb(216, 52, 79)"
                                  : "",
                              }}
                              placeholder={
                                errors.phone_number
                                  ? "Este campo es requerido*"
                                  : "Ingrese su numero telefónico"
                              }
                            />
                            <small>
                              <ErrorMessage
                                errors={errors}
                                name="phone_number"
                                render={({ message }) => <p>{message}</p>}
                              />
                            </small>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="form-group">
                      <label htmlFor="">Correo</label>
                      <input
                        type="text"
                        {...register("email", { required: true })}
                        style={{
                          border: errors.email
                            ? "3px solid rgb(216, 52, 79)"
                            : "",
                        }}
                        placeholder={
                          errors.email
                            ? "Este campo es requerido*"
                            : "Correo: ejemplo@dominio.com"
                        }
                      />
                      <small>
                        <ErrorMessage
                          errors={errors}
                          name="email"
                          render={({ message }) => <p>{message}</p>}
                        />
                      </small>
                    </div>

                    <div className="d-flex">
                      <div className="row ">
                        <div className="col-sm-12 col-md-6 col-lg-6">
                          <div className="form-group">
                            <label htmlFor="">Dirección</label>
                            <input
                              type="text"
                              {...register("location", { required: true })}
                              style={{
                                border: errors.location
                                  ? "3px solid rgb(216, 52, 79)"
                                  : "",
                              }}
                              placeholder={
                                errors.location
                                  ? "Este campo es requerido*"
                                  : "Ingrese su dirección"
                              }
                            />
                          </div>
                        </div>
                        <div className="col-sm-12 col-md-6 col-lg-6">
                          <div className="form-group">
                            <label htmlFor="">Responsable de pago</label>
                            <input
                              type="text"
                              {...register("payer", { required: false })}
                              style={{
                                border: errors.payer
                                  ? "3px solid rgb(216, 52, 79)"
                                  : "",
                              }}
                              placeholder={
                                errors.payer
                                  ? "Este campo es requerido*"
                                  : "Persona que realiza el pago"
                              }
                            />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="form-group">
                      <label htmlFor="">Comentario</label>
                      <input
                        type="text"
                        {...register("comments", { required: false })}
                        style={{
                          border: errors.comments
                            ? "3px solid rgb(216, 52, 79)"
                            : "",
                        }}
                        placeholder={
                          errors.comments
                            ? "Este campo es requerido*"
                            : "Agregue un comentario"
                        }
                      />
                      <small>
                        <ErrorMessage
                          errors={errors}
                          name="comments"
                          render={({ message }) => <p>{message}</p>}
                        />
                      </small>
                    </div>

                    <button
                      className="btn btn-cgray btn-block d-flex justify-content-center align-content-between"
                      type="submit"
                    >
                      <i className="material-symbols-sharp mr-1">save</i>
                      Agregar
                    </button>
                  </form>
                </div>
                <div className="panel-footer d-flex justify-content-center"></div>
              </div>
            </div>
          </div>
          <div></div>
        </div>
      </div>
      {/* <Footer></Footer> */}
    </React.Fragment>
  );
}
export default RegisterForm;