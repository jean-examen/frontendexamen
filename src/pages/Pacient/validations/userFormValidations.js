import * as yup from "yup";
const userSchema = yup
  .object({
    id_card: yup
      .string()
      .nullable()
      .matches(/^[1-9]\d*$/, "Solo debe contener digitos")
      .min(9, "Solo se permiten 9 digitos")
      .max(9, "Solo se permiten 9 digitos"),
    name: yup
      .string()
      .trim()
      .matches(/^[^ ]+$/, "No se permiten espacios en blanco")
      .matches(/^[aA-zZñÑ\u00C0-\u017F\s]+$/, "Solo se permiten letras ")
      .required(),
    last_name: yup
      .string()
      .trim()
      .matches(/^[aA-zZñÑ \u00C0-\u017F\s]+$/, "Solo se permiten letras ")
      .required(),
    email: yup
      .string()
      .matches(
        /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
        "Formato de correo invalido"
      )
      .email("Formato invalido")
      .required(""),
    phone_number: yup
      .string()
      .matches(/^[0-9]+$/, "Solo debe contener digitos")
      .matches(/^[1-9]\d*$/, "No debe iniciar con 0")
      .min(8, "Solo se permiten 8 digitos")
      .max(8, "Solo se permiten 8 digitos"),
    location: yup.string().required(),
    payer: yup
      .string()
      .trim()
      .matches(
        /^[aA-zZñÑ \u00C0-\u017F\s]+$/,
        "Only alphabets are allowed for this field "
      )
      .required(),
    comments: yup.string(),
  })
  .required();

export default userSchema;
