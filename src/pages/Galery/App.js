import React, { useEffect, useState } from "react";
import "./index.css";
import data from "./images";
import axios from "axios";
import { CompressorImage } from "../../Tools/tools";
import { customAlert } from "../../Tools/Alerts";
import { useParams } from "react-router-dom";
import Loader from "../../shared/loader";

let files = null;

function App() {
  const { id } = useParams();

  const [selectedImage, setSelectedImage] = useState("");
  const [modalState, setModalState] = useState(true);
  const [modalData, setModalData] = useState("");
  const [images, setImages] = useState([]);
  const [render, setRender] = useState(false);
  const [loadind, isLoading] = useState(false);

  useEffect(() => {
    setRender(true);
    const options = {
      method: "GET",
      url: `http://localhost/api Examen/image/list/${id}`,
    };

    axios
      .request(options)
      .then(function (response) {
        console.log(response);
        setImages(response.data);
        isLoading(true);
      })
      .catch(function (error) {
        console.error(error);
      });
    setRender(false);
  }, [render]);

  const onFileChange = async (e) => {
    files = e.target.files;
    files = files[0];
    if (files.size >= 5242880) {
      customAlert("error", "Solo se permiten archivos de maximo 5Mb");
      files = null;
      return;
    } else {
      if (files.size > 1048576) {
        files = await CompressorImage(files, 0.5);
        console.log(files.size);
      }
    }

    if (files.size !== 0) {
      let fileReader = new FileReader();
      fileReader.readAsDataURL(files);
      fileReader.onload = (event) => {
        setSelectedImage(event.target.result);
      };
    }
  };

  const showModal = (data) => {
    setModalState(false);
    setModalData(data);
  };

  const closeModal = () => {
    setModalState(true);
  };

  const onSubmit = () => {
    if (selectedImage !== "") {
      const formData = {
        id_user: id,
        image: selectedImage,
      };

      let endpoint = "http://localhost/api Examen/image/create";
      axios.post(endpoint, formData, {}).then((res) => {
        customAlert("success", "", res.data["Message"]);
      });
    } else {
      customAlert("error", "", "Seleccione una imagen");
    }

    files = null;
    setSelectedImage("");
  };

  return (
    <>
      <div hidden={!modalState} className="App_gallery">
        <div className="mt-3">
          <div className="col-sm-12 col-md-12 col-lg-12 col-xl-10 mx-auto">
            <div className="chooser_container">
              <input type="file" onChange={onFileChange} />
              <button name="btn_upload" className="btn btn_ligth_blur" onClick={onSubmit}>
                {" "}
                <i className="fa-solid fa-file-arrow-up"></i>
              </button>
            </div>

            <div className="gallery">
              {images.status === 0 ? (
                <div>"Sin datos para mostrar"</div>
              ) : (
                <>
                  {images.map((item, index) => {
                    return (
                      <div key={index} className="card_gallery">
                        <img src={item.image_path} alt="gallery" />
                        <div className="button-menu">
                          <button className="right-aligned-button btn-card">
                            <i className="fa-solid fa-trash-can"></i>
                          </button>
                          <button
                            onClick={() => showModal(item.image_path)}
                            className="right-aligned-button btn-card"
                          >
                            <i className="fa-solid fa-expand"></i>
                          </button>
                        </div>
                      </div>
                    );
                  })}
                </>
              )}

              {/* {images[0] ? (
                <div></div>
              ) : (
                <div className="w-10 h-10">
                  <Loader w="300px" h="300px" bgColor=""></Loader>
                </div>
              )} */}
            </div>
          </div>
        </div>
      </div>

      <div hidden={modalState} className="gallery_modal">
        <div className="row">
          <button className="btn-card m-3" onClick={() => closeModal()}>
            <i className="fa-solid fa-compress"></i>
          </button>
        </div>

        <div className="col-sm-12 col-md-12 ">
          <div className="modal_image_container mx-auto">
            <img className="mx-auto" src={modalData} alt="gallery" />
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
