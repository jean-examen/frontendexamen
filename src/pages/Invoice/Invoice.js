import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { ErrorMessage } from "@hookform/error-message";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import userSchema from "./validations/userFormValidations";

export default function Invoice() {
  const { id } = useParams();
  const [patient, setPatient] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [info, setInfo] = useState("");
  const [amount, setAmount] = useState("");

  const {
    register,
    handleSubmit,
    getValues,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(userSchema),
  });

  useEffect(() => {
    document.title= `Factura`;

    const options = {
      method: "GET",
      url: `http://localhost/api Examen/patient/findById/${id}`,
    };

    axios
      .request(options)
      .then(function (response) {
        setPatient(response.data[0]);
      })
      .catch(function (error) {
        console.error(error);
      });
  }, []);

  const sendWAClick = (dataForm) => {
    console.log("DataForm", dataForm);
    var message = createMessage(dataForm);
    alert("Enviado a wa: " + message);
    window.open(
      `https://api.whatsapp.com/send?phone=506${dataForm.phone}&text=${message}`,
      "_blank"
    );
  };

  const sendEmailClick = () => {
    alert("Enviado a mail " + email);
  };

  const createMessage = (formData) => {
    return (
      "\n CÉDULA: " +
      patient.id_card +
      "\n NOMBRE: " +
      patient.name +
      "\n APELLIDO: " +
      patient.last_name +
      "\n DIRECCIÓN: " +
      formData.email +
      "\n TELÉFONO: " +
      formData.phone +
      "\n TRATAMIENTOS: " +
      formData.info +
      "\n Monto: " +
      formData.amount
    );
  };

  const validate = () => {};

  return (
    <>
      <div className="App_factura">
        <div className="container mt-5">
          <div className="card">
            <div className="card-body">
              <div className="info-card">
                <h3>Datos</h3>
                <ul>
                  <li>
                    <strong>CÉDULA: </strong> {patient.id_card}
                  </li>
                  <li>
                    <strong>NOMBRE: </strong> {patient.name}
                  </li>
                  <li>
                    <strong>APELLIDO: </strong> {patient.last_name}
                  </li>
                  <li>
                    <strong>EMAIL: </strong> {patient.email}
                  </li>
                  <li>
                    <strong>TELÉFONO: </strong> {patient.phone_number}
                  </li>
                </ul>
                <hr></hr>

                <div className="row">
                  <div className="col-sm-4 form-group mt-3">
                    <label>Monto total</label>
                    <input
                      className="form-control"
                      {...register("amount", { required: true })}
                      style={{
                        border: errors.amount
                          ? "3px solid rgb(216, 52, 79)"
                          : "",
                      }}
                      placeholder={
                        errors.amount ? "Este campo es requerido*" : "Ej 20000"
                      }
                    />
                    <small>
                      <ErrorMessage
                        errors={errors}
                        name="amount"
                        render={({ message }) => <p>{message}</p>}
                      />
                    </small>
                  </div>

                  <div className="col-sm-4 form-group mt-3">
                    <label>Correo</label>
                    <input
                      className="form-control"
                      defaultValue={patient.email}
                      {...register("email", { required: true })}
                      style={{
                        border: errors.email
                          ? "3px solid rgb(216, 52, 79)"
                          : "",
                      }}
                      placeholder={
                        errors.email ? "Este campo es requerido*" : "Ej 20000"
                      }
                    ></input>
                    <small>
                      <ErrorMessage
                        errors={errors}
                        name="email"
                        render={({ message }) => <p>{message}</p>}
                      />
                    </small>
                  </div>

                  <div className="col-sm-4 form-group mt-3">
                    <label>Teléfono</label>

                    <input
                      className="form-control"
                      defaultValue={patient.phone_number}
                      placeholder="Numero telefoníco a enviar"
                      {...register("phone", { required: true })}
                    ></input>
                    <small>
                      <ErrorMessage
                        errors={errors}
                        name="phone"
                        render={({ message }) => <p>{message}</p>}
                      />
                    </small>
                  </div>

                  <div className="col-12 form-group mt-3">
                    <label>Tratamientos aplicados</label>
                    <textarea
                      className="form-control"
                      {...register("info", { required: true })}
                    ></textarea>
                  </div>
                </div>

                <button
                  onClick={handleSubmit(sendWAClick)}
                  className="btn btn_ligth_blur"
                >
                  <i className="fa-brands fa-whatsapp"></i>
                </button>

                <button onClick={sendEmailClick} className="btn btn_ligth_blur">
                  <i className="fa-regular fa-envelope"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
