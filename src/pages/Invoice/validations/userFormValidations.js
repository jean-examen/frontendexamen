import * as yup from "yup";
const userSchema = yup
  .object({
    amount: yup
    .string()
    .matches(/^[1-9]\d*$/, "Solo debe contener digitos"),

    email: yup
    .string().email("Formato invalido").required(""),

    phone: yup
      .string()
      .matches(/^[0-9]+$/, "Solo debe contener digitos")
      .min(8, "Solo se permiten 8 digitos")
      .max(8, "Solo se permiten 8 digitos"),
  
  })
  .required();

export default userSchema;

