import React from 'react';
import { Link } from "react-router-dom";

class SideMenu extends React.Component {
    

    render() {
        return (
            <>
                <div className="offcanvas-header">
                    <h5 className="ofcanvas-title">Menu</h5>
                    <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas"></button>
                </div>
                <div className="offcanvas-body">
                    <div>
                        <p>This menu is to have a better navigation through the web page.</p>
                    </div>

                    <div className="form-outline mb-2">
                        <Link type="button" className="btn btn-primary" to={'/edit-profile'}>Edit Profile</Link>
                    </div>

                    <div className="form-outline mb-2">
                        <Link type="button" className="btn btn-primary" to={'/ticket-shop'}>Ticket Shop​</Link>
                    </div>

                    <div className="form-outline mb-2">
                        <Link type="button" className="btn btn-primary" to={'/games-history'}>Last 5 Games​</Link>
                    </div>

                    <div className="form-outline mb-2">
                        <Link type="button" className="btn btn-primary" to={'/games-in-queue'}>Games In queue​</Link>
                    </div>

                    <div className="form-outline mb-2">
                        <Link type="button" className="btn btn-primary" to={'/my-games'}>My Games</Link>
                    </div>
                </div>

            </>
        );
    }
}

export default SideMenu;