import Swal from "sweetalert2";

export function customAlert(icon, title, text) {
  Swal.fire({
    icon: icon,
    title: title,
    text: text,
  });
}
