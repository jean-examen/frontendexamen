import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

// Imports Paginas
// import EmploysList from "./pages/Employs/Empleados";
// import Changepassword from "./pages/ChangePassword/Changepassword";
// //import Empleados from './pages/Employs/Employs';
// import ForgotPassword from "./pages/ForgotPassword/ForgotPassword";
// import Galery from "./pages/Galery/Galery";
// import History from "./pages/History/History";
import Home from "./pages/Home/Home";
import Login from "./pages/Login/Login";
// import Payment_Info from "./pages/Payment_Info/Payment_Info";
// import Record from "./pages/Record/Record";
// import Register from "./pages/Register/Register";
// import Schedule from "./pages/Schedule/Schedule";
// import Header from "./pages/GeneralStructure/Header";
import Appointment from "./pages/appointment/Appointment";
import AppointmentEdit from "./pages/appointment/AppointmentEdit";
import Patient from "./pages/Pacient/index";
import PatientCreate from "./pages/Pacient/Register";
import PatientEdit from "./pages/Pacient/Edit";
import { ToastContainer } from "react-toastify";
import Loader from "./shared/loader/index";
import Navbar from "./shared/Navbar";
import Invoice from "./pages/Invoice/Invoice";
import Record from "./pages/Record/Record";
import Gallery from "./pages/Galery/App";

function App() {
  return (
    <Router>
      <Navbar></Navbar>
      <ToastContainer />
      <Routes>
        <Route path={"/"} element={<Home />} />
        <Route path={"/Home"} element={<Home />} />
        <Route path={"/login"} element={<Login />} />

        <Route path={"/patient/list"} element={<Patient />} />
        <Route path={"/patient/create"} element={<PatientCreate />} />
        <Route path={"/patient/edit/:id"} element={<PatientEdit />} />
        <Route path={"/appointment/:id"} element={<Appointment />} />
        <Route path={"/appointment/edit/:id"} element={<AppointmentEdit />} />
        <Route path={"/loader"} element={<Loader h="100vh" w="100vw" bgColor="blue" />} />
        <Route path={"/navbar"} element={<Navbar />} />
        <Route path={"/factura/:id"} element={<Invoice />} />
        <Route path={"/record/:id"} element={<Record />} />
        <Route path={"/patient/gallery/:id"} element={<Gallery />} />
      </Routes>
    </Router>
  );
}

export default App;
